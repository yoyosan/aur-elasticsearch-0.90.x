# $Id$
# Maintainer: Massimiliano Torromeo <massimiliano.torromeo@gmail.com>
# Contributor: Marcello "mererghost" Rocha <https://github.com/mereghost>
# Refactored by Blaž "Speed" Hrastnik <https://github.com/archSeer>

pkgname=elasticsearch
pkgver=0.90.13
pkgrel=1
pkgdesc="Distributed RESTful search engine built on top of Lucene"
arch=('i686' 'x86_64')
url="http://www.elasticsearch.org/"
license=('APACHE')
depends=('java-runtime' 'systemd')
install='elasticsearch.install'
source=(
  "http://download.elasticsearch.org/$pkgname/$pkgname/$pkgname-$pkgver.tar.gz"
  elasticsearch.service
  elasticsearch@.service
  elasticsearch-sysctl.conf
  elasticsearch-tmpfile.conf
  elasticsearch.default
)
sha256sums=('82904d4c564fc893a1cfc0deb4526073900072a3f4667b995881a2ec5cdfdb43'
            'f3a6e5b968ffe38edc42c80193f75ab1682a5224c977c8957816d321733aabc4'
            'c3ea4d82da44da8ea9bcb4d7d9cdfb086d25fd8ec2ea457adec290f0d6ebe083'
            'b3feb1e9c7e7ce6b33cea6c727728ed700332aae942ca475c3bcc1d56b9f113c'
            '39ab5801b45c0f49af43c4d1826a655a981bfa07e4b3791d7a0719f8c191d8d9'
            'a0ee0761156a02d3f79db5ef295613c1a6d4267482c8db889d92a94b59feb497')

backup=('etc/elasticsearch/elasticsearch.yml'
        'etc/elasticsearch/logging.yml'
        'etc/default/elasticsearch')

prepare() {
  cd "$srcdir"/$pkgname-$pkgver

  for script in plugin elasticsearch; do
    sed 's|^ES_HOME=.*dirname.*|ES_HOME=/usr/share/elasticsearch|' \
      -i bin/$script

    for java in jre jdk; do
      sed "/^CDPATH/i [ -f /etc/profile.d/$java.sh ] && . /etc/profile.d/$java.sh" \
        -i bin/$script
    done
  done

  sed 's|$ES_HOME/lib|/usr/lib/elasticsearch|g' -i bin/elasticsearch.in.sh bin/plugin

  echo -e '\nJAVA_OPTS="$JAVA_OPTS -Des.path.conf=/etc/elasticsearch"' >> bin/elasticsearch.in.sh

  sed -re 's;#\s*(path\.conf:).*$;\1 /etc/elasticsearch;' \
    -e '0,/#\s*(path\.data:).*$/s;;\1 /var/lib/elasticsearch;' \
    -e 's;#\s*(path\.work:).*$;\1 /tmp/elasticsearch;' \
    -e 's;#\s*(path\.logs:).*$;\1 /var/log/elasticsearch;' \
    -i config/elasticsearch.yml
}

package() {
  cd "$srcdir"/$pkgname-$pkgver
  install -dm755 "$pkgdir"/etc/elasticsearch

  if [ $CARCH = 'x86_64' ]; then
    install -Dm644 lib/sigar/libsigar-amd64-linux.so "$pkgdir"/usr/lib/elasticsearch/sigar/libsigar-amd64-linux.so
  else
    install -Dm644 lib/sigar/libsigar-x86-linux.so "$pkgdir"/usr/lib/elasticsearch/sigar/libsigar-x86-linux.so
  fi
  cp lib/sigar/sigar*.jar "$pkgdir"/usr/lib/elasticsearch/sigar/
  cp lib/*.jar "$pkgdir"/usr/lib/elasticsearch/

  cp config/* "$pkgdir"/etc/elasticsearch/

  install -Dm755 bin/elasticsearch "$pkgdir"/usr/bin/elasticsearch
  install -Dm755 bin/plugin "$pkgdir"/usr/bin/elasticsearch-plugin
  install -Dm644 bin/elasticsearch.in.sh "$pkgdir"/usr/share/elasticsearch/elasticsearch.in.sh

  install -Dm644 "$srcdir"/elasticsearch.service "$pkgdir"/usr/lib/systemd/system/elasticsearch.service
  install -Dm644 "$srcdir"/elasticsearch@.service "$pkgdir"/usr/lib/systemd/system/elasticsearch@.service
  install -Dm644 "$srcdir"/elasticsearch-tmpfile.conf "$pkgdir"/usr/lib/tmpfiles.d/elasticsearch.conf
  install -Dm644 "$srcdir"/elasticsearch-sysctl.conf "$pkgdir"/usr/lib/sysctl.d/elasticsearch.conf

  install -Dm644 "$srcdir"/elasticsearch.default "$pkgdir"/etc/default/elasticsearch

  ln -s ../../../var/lib/elasticsearch "$pkgdir"/usr/share/elasticsearch/data
}

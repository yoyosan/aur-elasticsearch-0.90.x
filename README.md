# About

This is an AUR build package for elasticsearch version 0.90.x.

It is based on the Arch community package, [elasticsearch](https://www.archlinux.org/packages/community/x86_64/elasticsearch/).

To check the latest version of elasticsearch 0.90, go to http://www.elasticsearch.org/downloads/page/2/.

# Building/Installation

```
git clone https://yoyosan@bitbucket.org/yoyosan/aur-elasticsearch-0.90.x.git
cd aur-elasticsearch-0.90.x
makepkg -s
sudo pacman -U *xz
```

# Have fun!